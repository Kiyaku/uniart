using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


/*
 * Nodes are used to modify things, like vertices, handles, etc
 * They can be dragged. When the user selects one, a handle might show up
*/

[RequireComponent(typeof(BoxCollider))]
public class Node : MonoBehaviour {
	public Transform Target;
	
	private Action OnModified;
	public Action<Transform> OnDelete;
	
	public enum State {
		NORMAL,
		HOVER,
		SELECTED,
		DISABLED
	}
	
	public State CurrentState = State.NORMAL;
	
	
	public void AssignTarget(Transform target, Action onModified, Action<Transform> onDelete = null) {
		Target = target;
		OnModified = onModified;
		OnDelete = onDelete;
		transform.position = Target.position + new Vector3(0, 0, -1);
		renderer.enabled = false;
		CurrentState = State.NORMAL;
	}
	

	public void Select() {
		CurrentState = State.HOVER;
	}
	
	
	public void Deselect() {
		CurrentState = State.NORMAL;
	}
	
	
	private void MoveNode(Vector3 pos) {
		transform.position = pos;
		Target.position = transform.position;
		
		if(OnModified != null)
			OnModified();
		
		Target.SendMessage("UpdatePosition", SendMessageOptions.DontRequireReceiver);
	}
	
	
	private void OnMouseOver() {
		if(Input.GetMouseButtonDown(1)) {
			if(OnDelete != null)
				OnDelete(Target);
			
			NodeManager.Ins.RemoveNode(this);
		}
	}
	
	
	private void OnMouseUp() {
		Deselect();
	}
	
	
	private void OnMouseEnter() {
		Select();
	}
	
	
	private void OnMouseExit() {
		Deselect();
	}
	
	
	private void OnMouseDrag() {
        Vector3 mouseToPos = AtlasEditor.Ins.camera.ScreenToWorldPoint(Input.mousePosition);
		mouseToPos.z = 0;
		
		Select();
		
		MoveNode(mouseToPos);
    }
}
