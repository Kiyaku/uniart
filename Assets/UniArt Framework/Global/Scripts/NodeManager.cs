using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class NodeManager : Singleton<NodeManager> {
	public Transform NodePrefab;
	public Texture2D NodeTextureNormal;
	public Texture2D NodeTextureSelected;
	
	private Dictionary<Transform, Node> NodeList = new Dictionary<Transform, Node>();
	private bool showTempNode = false;
	private Vector3 tempNodePos = Vector3.zero;
	
	
	public void AssignNode(Transform target, Action onModified, Action<Transform> onDelete = null) {
		// If we already have a node for this target, keep it
		if(NodeList.ContainsKey(target)) {
			if(NodeList[target].gameObject.active)
				return;
			
			NodeList[target].gameObject.SetActiveRecursively(true);
			NodeList[target].AssignTarget(target, onModified, onDelete);
			return;
		}
		
		foreach(KeyValuePair<Transform, Node> kpv in NodeList) {
			if(!kpv.Value.gameObject.active && !NodeList.ContainsKey(target)) {
				Node node = kpv.Value;
				NodeList.Remove(kpv.Key);
				NodeList.Add(target, node);
				
				node.gameObject.SetActiveRecursively(true);
				node.AssignTarget(target, onModified, onDelete);
				return;
			}
		}
		
		Transform newNode = (Transform)Instantiate(NodePrefab);
		newNode.parent = transform;
		NodeList.Add(target, newNode.GetComponent<Node>());
		
		newNode.GetComponent<Node>().AssignTarget(target, onModified, onDelete);
	}
	
	
	public void ShowTempNode(Vector3 pos) {
		showTempNode = true;
		tempNodePos = pos;
	}
	
	
	public void UpdateNodes() {
		foreach(KeyValuePair<Transform, Node> kpv in NodeList) {
			if(kpv.Value.Target == null) {
				// Delete node and check for dead notes again
				RemoveNode(kpv.Value);
			}
		}
	}
	
	
	// Removes one specific node and hides it
	public void RemoveNode(Node node) {
		node.Target = null;
		node.gameObject.SetActiveRecursively(false);
	}
	
	
	// Removes every node and hides them
	public void RemoveAllNodes() {
		foreach(KeyValuePair<Transform, Node> kpv in NodeList) {
			RemoveNode(kpv.Value);
		}
	}
	
	
	private void OnGUI() {
		GUI.color = Color.white;
		
		if(NodeTextureNormal != null && NodeTextureSelected != null) {
			foreach(KeyValuePair<Transform, Node> kpv in NodeList) {
				if(kpv.Value.gameObject.active) {
					Vector3 screenPos = AtlasEditor.AtlasCam.WorldToScreenPoint(kpv.Value.transform.position);
					GUI.DrawTexture(new Rect(screenPos.x - 8, Screen.height - screenPos.y - 8, 16, 16), kpv.Value.CurrentState == Node.State.NORMAL ? NodeTextureNormal : NodeTextureSelected);
				}
			}
		}
		
		if(showTempNode) {
			Vector3 screenPos = AtlasEditor.AtlasCam.WorldToScreenPoint(tempNodePos);
			GUI.color = new Color(1, 1, 1, 0.4f);
			GUI.DrawTexture(new Rect(screenPos.x - 8, Screen.height - screenPos.y - 8, 16, 16), NodeTextureNormal);
			
			if(Event.current.type == EventType.Repaint)
				showTempNode = false;
		}
	}
}
