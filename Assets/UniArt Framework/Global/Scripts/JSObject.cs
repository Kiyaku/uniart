using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class JSObject {
	private static int ID_COUNT = 0;
	private int _id = -1;
	public int id {
		get {
			if( _id == -1 ) _id = ID_COUNT++;
			
			return _id;
		}
	}
	
	public bool isArrayList = false; // hack, but used to preserve array formatting =(
	public Hashtable data;
	public int length {
		get { return data.Count; }
	}

	public JSObject() {
		data = new Hashtable();
	}
		
	public void Clear() {
		data.Clear();
	}
	
	// intended for use only for arraylists. but it also works otherwise.
	public JSObject this[ int index ] {
		get {
			return this[index.ToString()];
		}
		set {
			this[index.ToString()] = value;
		}
	}
			
	public JSObject this[ string key ] {
		get {
			if( !data.ContainsKey( key ) ) {
//				Debug.LogWarning( "[JSObject] key \""+key+"\" not found; creating entry." );
				data[key] = new Hashtable();
			}
			
			return Get<JSObject>( key );
		}
		set {
			Set( key, value );
		}
	}
	
	public object Get( string key ) {
		return (key != null && data.ContainsKey( key ))? data[key] : null;
	}
	
	public T Get<T>( string key ) {
		if( string.IsNullOrEmpty( key ) ) {
			//Debug.LogWarning( "key is null or empty!" );
			return default(T);
		}
		
		if((data.ContainsKey( key ) && data[key] != null)) {
			if(data[key] is T) {
				if( typeof(T) == typeof(ArrayList) ) {
					// if asking for an ArrayList, return an ArrayList of JSObjects (rather than Hashtables)
					// for the sake of consistency.
					ArrayList vals = data[key] as ArrayList;
					ArrayList toReturn = new ArrayList();
					for( int a = 0 ; a < vals.Count ; a++ ) {
						JSObject datum = new JSObject();
						if(vals[a].GetType() == typeof(Hashtable)) {
							datum.data = vals[a] as Hashtable;
							toReturn.Add( datum );		
						} else {
							toReturn.Add(vals[a]);
						}
						
					}
					return (T)(object)toReturn;
				} else
					return (T)(data[key]);	
			} else {
				if(typeof(T) == typeof(JSObject) ) {
					JSObject jso = new JSObject();
					if( data[key] is Hashtable ) {
						jso.data = (Hashtable)data[key];
					} else if( data[key] is ArrayList ) {
						jso = ArrayListToJSObject( (ArrayList)(data[key]) );
					}
					
					return (T)(object)jso;
				} else if(typeof(T) == typeof(Int32) && data[key] is Double) {
					return (T)(object)Convert.ToInt32(data[key]);
				}
				//Debug.LogWarning( "Wrong Type! Type is " + data[key].GetType()+". Expected "+ typeof(T).ToString());
			}
		}
		return default(T);
	}
		
	public void Set( string key, object value ) {
		if( string.IsNullOrEmpty( key ) ) {
			//Debug.LogError( "[JSObject] key cannot be null or empty!" );
			return;
		}
		
		if( value == null ) {
			// if it exists, remove it
			if( data.ContainsKey( key ) )
				data.Remove(key);
			
			// bail out. don't put a null/empty value in
			return;
		}
		
		if(value is JSObject) {
			data[key] = ((JSObject)value).data;
		} else if( value is ArrayList ) {
			ArrayList alist = new ArrayList();
			for( int a = 0 ; a < (value as ArrayList).Count ; a++ ) {
				var val = (value as ArrayList)[a];
				if( val is JSObject )
					val = ((JSObject)val).data;
				
				alist.Add( val );
			}
			data[key] = alist;
		} else {
			data[key] = value;	
		}
	}
		
	public bool Has( string key ) {
		return string.IsNullOrEmpty( key ) ? false : data.ContainsKey( key );
	}
		
	public ICollection GetKeys() {
		return data.Keys;
	}
		
	public ICollection GetValues() {
		return data.Values;
	}
		
	public static JSObject FromJSON( string json ) {
		object obj = MiniJSON.jsonDecode(json);
		JSObject jso = new JSObject();
		if( obj is ArrayList) {
			jso = ArrayListToJSObject( (ArrayList)obj );
		} else
			jso.data = (Hashtable)obj;

		if( jso.data == null ) {
			//Debug.Log( "invalid json for parsing: "+json );
			jso = null;
		}
		
		return jso;
	}
		
	public string ToJSON() {
		if( !isArrayList ) {
			return data.toJson();
		}
		
		// preserve array text formatting
		StringBuilder sb = new StringBuilder();
		foreach( string key in data.Keys ) {
			JSObject jsob = new JSObject();
			if( data[key] is ArrayList ) {
				jsob = ArrayListToJSObject( (ArrayList)(data[key]) );
			} else {
				jsob.data = data[key] as Hashtable;
			}
			
			sb.Append( jsob.ToJSON() );
			sb.Append( ',' );
		}
		
		if( sb.Length > 0 ) {
			sb.Insert( 0, '[' );
			sb[sb.Length-1] = ']';
		} else {
			sb.Append( "[]" );
		}
		
		return sb.ToString();
	}
	
	public override string ToString() {
		return ToJSON();
	}

	/// <summary>
	/// Assumes 
	///  - the json object is simply an array of numbered indicies { 0:value, 1:value, etc. }
	///  - that all of the values within that array are of the same type
	///  Returns an array for all the values in the JSObject.
	/// </summary>
	/// <returns>
	/// The array.
	/// </returns>
	/// <typeparam name='T'>
	/// The type the array consists of.
	/// </typeparam>
	public T[] BuildToArray<T>()
	{
		// assumes all the entries in the object is part of the array.
		ICollection keys = this.GetKeys();
		T[] returnAry = new T[keys.Count];
		foreach( string key in keys ) 
		{
			int index = int.Parse(key);
			T val = this.Get<T>(key);
			returnAry[index] = val;
		}
		
		return returnAry;
	}
	
	public static JSObject ArrayListToJSObject( ArrayList list ) {
		JSObject js = new JSObject();
		Hashtable ht = new Hashtable();
		for( int a = 0 ; a < list.Count ; a++ ) {
			ht[a.ToString()] = (list[a] is JSObject)? ((JSObject)list[a]).data : list[a];
		}
		js.data = ht;
		js.isArrayList = true;
		return js;
	}
}