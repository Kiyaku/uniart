using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ModelToTexture : Singleton<ModelToTexture> {
	private Camera cam;
	private float alpha = 1.0f;
	private Material mat;
	
	
	private void SetupCamera() {
		setMat();
		cam = gameObject.AddComponent<Camera>();
		cam.enabled = false;
		
		cam.orthographic = true;
		cam.cullingMask = 1 << LayerMask.NameToLayer("ModelToTexture");
		cam.clearFlags = CameraClearFlags.Depth;
		transform.position = new Vector3(10000, 10000, -10);
	}
	
	
	public Texture2D CreateTexture(Transform model) {
		if(model == null || model.collider == null)
			return null;
		
		if(cam == null) {
			SetupCamera();
		}
		
		cam.enabled = true;
		
		// store starter values
		int layerMask = model.gameObject.layer;
		Vector3 tempPos = model.position;
		Vector3 localScale = model.localScale;
		
		float height = Camera.main.orthographicSize * 2.0f;
    	float width = height * (float)Screen.width / (float)Screen.height;
		
		if(model.collider.bounds.size.x > model.collider.bounds.size.y)		
			model.localScale = Vector3.one * (width / model.collider.bounds.size.x);
		else
			model.localScale = Vector3.one * (height / model.collider.bounds.size.y);
		
		model.position = new Vector3(10000, 10000, 0) - model.collider.bounds.center;
		model.gameObject.layer = LayerMask.NameToLayer("ModelToTexture");
		
		int resWidth = Screen.width / 10;
		int resHeight = Screen.height / 10;
		
		RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
        cam.targetTexture = rt;
		Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.ARGB32, false);
		cam.Render();
        RenderTexture.active = rt;
		
        screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
		screenShot.Apply();
		cam.enabled = false;
		
		model.localScale = localScale;
		model.gameObject.layer = layerMask;
		model.position = tempPos;
		
		return screenShot;
	}
	
	
	// Temporary way of removing the screenshot alpha
	// TODO: Take screenshot with proper alpha
	private void setMat ()
	{
	    mat = new Material(
	        "Shader \"Hidden/Clear Alpha\" {" +
	        "Properties { _Alpha(\"Alpha\", Float)=1.0 } " +
	        "SubShader {" +
	        "    Pass {" +
	        "        ZTest Always Cull Off ZWrite Off" +
	        "        ColorMask A" +
	        "        SetTexture [_Dummy] {" +
	        "            constantColor(0,0,0,[_Alpha]) combine constant }" +
	        "    }" +
	        "}" +
	        "}"
	    );
	}
	
	
	private void OnPostRender()
	{
	    GL.PushMatrix();
	    GL.LoadOrtho();
	    mat.SetFloat( "_Alpha", alpha );
	    mat.SetPass(0);
	    GL.Begin( GL.QUADS );
	    GL.Vertex3( 0, 0, 0.1f );
	    GL.Vertex3( 1, 0, 0.1f );
	    GL.Vertex3( 1, 1, 0.1f );
	    GL.Vertex3( 0, 1, 0.1f );
	    GL.End();
	    GL.PopMatrix();
	}
}
