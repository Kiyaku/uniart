using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;


public class AtlasEditor : Singleton<AtlasEditor> {
	public Transform GridBGPrefab;
	public float MouseSpeed = 200;
	public Color ColShapeNormal = new Color(0.4f, 0.4f, 1f);
	public Color ColShapeSelected = new Color(0.1f, 0.8f, 0.3f);
	public GUISkin Skin;
	public Material BoneMaterial;
	
	public static Camera AtlasCam;
	private Material GLMat;
	private Vector2 workAreaSize = new Vector2(128, 128);
	private Texture2D loadedTexture = null;
	private Transform imagePlaceholder;
	private Transform imageGrid;
	private Material imagePlaceholderMat;
	private Transform gridBG;
	
	private List<Shape> shapeList = new List<Shape>();
	private List<Vector3> tempVertexList = new List<Vector3>();
	private Shape selectedShape;
	
	private Rect shapeWindowRect;
	private Atlas currentAtlas;
	
	private List<Vector3> tempBonePos = new List<Vector3>();
	
	
	public Material AtlasMat {
		get { return imagePlaceholderMat; }
	}
	
	private enum Modes {
		VIEW,
		ADD_SIMPLE_SHAPE, 
		ADD_COMPLEX_SHAPE
	}
	
	
	private enum ShapeViewMode {
		OUTLINE = 0,
		WIREFRAME = 1,
		MESH = 2
	}
	
	
	private Modes currentMode = Modes.VIEW;
	private ShapeViewMode currentShapeViewMode = ShapeViewMode.MESH;
	
	
	void Awake() {
		Initialize();
	}
	
	
	public void Initialize() {
		shapeWindowRect = new Rect(Screen.width - 160, 10, 150, 400);
		
		CreateImagePlanes();
		SetupCamera();
		CreateGLMat();
		
		PreLoadAtlas();
		
		ShapeManager.onDeleteShape += DeleteShape;
	}
	
	
	// Checks if there is an available atlas map. If so, load it
	private void PreLoadAtlas() {
		if(PlayerPrefs.HasKey("latestAtlasPath")) {
			LoadAtlas(AtlasManager.Ins.LoadAtlas(PlayerPrefs.GetString("latestAtlasPath")));
		}
	}
	
	
	// Create planes for image texture and BG
	private void CreateImagePlanes() {
		imagePlaceholderMat = new Material(Shader.Find("Transparent/VertexLit"));
		
		imagePlaceholder = GameObject.CreatePrimitive(PrimitiveType.Plane).transform;
		Destroy (imagePlaceholder.collider);
		imagePlaceholder.eulerAngles = new Vector3(90, 180, 0);
		imagePlaceholder.renderer.material = imagePlaceholderMat;
		imagePlaceholder.gameObject.layer = LayerMask.NameToLayer("Animator");
		imagePlaceholder.position = new Vector3(0, 0, 10);
		imagePlaceholder.parent = transform;
		
		gridBG = Instantiate(GridBGPrefab) as Transform;
		gridBG.position = new Vector3(0, 0, 11);
		gridBG.parent = transform;
		
		UpdateWorkAreaSize(new Vector3(workAreaSize.x, 1, workAreaSize.y) / 10f);
	}
	
	
	// Resizes work area depending on texture size
	private void UpdateWorkAreaSize(Vector3 size) {
		imagePlaceholder.localScale = size;
		gridBG.localScale = size;
	}
	
	
	void Update() {
		if(AtlasCam != null) {
			if(Input.GetKey(KeyCode.UpArrow)) {
				AtlasCam.orthographicSize--;
			}
			
			if(Input.GetKey(KeyCode.DownArrow)) {
				AtlasCam.orthographicSize++;
			}
			
			AtlasCam.orthographicSize -= Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * MouseSpeed;
			
			if(AtlasCam.orthographicSize < 10)
				AtlasCam.orthographicSize = 10;
		}
		
		
		// Deselect shapes when pressing Escape
		if(Input.GetKeyDown(KeyCode.Escape)) {
			DeselectShape();
		}
		
		
		// Left click to add new node, enter to finish shape
		if(currentMode == Modes.ADD_COMPLEX_SHAPE) {
			if(Input.GetMouseButtonDown(0)) {
				Vector3 mouseToWorld = AtlasCam.ScreenToWorldPoint(Input.mousePosition);
				tempVertexList.Add(new Vector3(mouseToWorld.x, mouseToWorld.y, 0));
			}
			
			if(Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return)) {
				currentMode = Modes.VIEW;
				
				Shape shape = new Shape(Shape.ShapeType.COMPLEX, tempVertexList);
				shape.SetLayer("Animator");
				shape.SetMaterial(imagePlaceholderMat);
				shape.Parent.parent = transform;
				selectedShape = shape;
				
				shapeList.Add(shape);
				
				SelectShape(selectedShape);
				
				currentAtlas.SetShapes(shapeList);
			}
		}
		
		/*
		if(currentMode == Modes.ADD_SIMPLE_SHAPE) {
			if(Input.GetMouseButtonDown(0) && tempBonePos.Count == 0) {
				// Create first bone
				Debug.LogError("Add Position");
				Vector3 mouseToWorld = AnimCam.ScreenToWorldPoint(Input.mousePosition);
				tempBonePos.Add(new Vector3(mouseToWorld.x, mouseToWorld.y, 0));
			}
			
			if(Input.GetMouseButtonUp(0) && tempBonePos.Count == 1) {
				// Finish Shape
				Vector3 mouseToWorld = AnimCam.ScreenToWorldPoint(Input.mousePosition);
				tempBonePos.Add(new Vector3(mouseToWorld.x, mouseToWorld.y, 0));
			}
		}
		*/
		
		if(currentMode == Modes.VIEW) {
			ScanForShapes();
		}
		
		
		if(selectedShape != null) {
			if(Input.GetKey(KeyCode.Delete)) {
				DeleteShape(selectedShape);
				selectedShape = null;
			}
			
			if(Input.GetKey(KeyCode.LeftShift)) {
				DrawTempNewNode();
			}
			
			if(Input.GetKey(KeyCode.LeftControl) && Input.GetMouseButtonDown(0)) {
				Vector3 mouseToWorld = AtlasCam.ScreenToWorldPoint(Input.mousePosition);
				mouseToWorld.z = 0;
				selectedShape.MovePivot(mouseToWorld);
			}
		}
	}
	
	
	// Checks if the user clicks a shape, if so, select it
	private void ScanForShapes() {
		Ray ray = AtlasCam.ScreenPointToRay(Input.mousePosition);
		ray.direction = new Vector3(0, 0, 1);
		RaycastHit hit;
		
		if(new Rect(0, 0, 120, Screen.height).Contains(Input.mousePosition))
			return;
		
		if(Input.GetMouseButtonDown(0)) {
			if(Physics.Raycast(ray, out hit, 10000)) {
	            if(hit.transform.tag == "Shape") {
					foreach(Shape shape in shapeList) {
						if(shape.Parent == hit.transform) {
							DeselectShape();
							SelectShape(shape);
							return;
						}
					}
				}
			} else {
				DeselectShape();
			}
		}
	}
	
	
	// Unselects current shape
	private void DeselectShape() {
		imagePlaceholder.renderer.material.SetColor("_Color", new Color(1, 1, 1, 1f));
			
		foreach(Shape s in shapeList) {
			s.Parent.renderer.material.SetColor("_Color", new Color(1, 1, 1, 1f));
		}
		
		if(selectedShape != null) {
			selectedShape = null;
			NodeManager.Ins.RemoveAllNodes();
		}
	}
	
	
	// Select shape and change color
	private void SelectShape(Shape shape) {
		imagePlaceholder.renderer.material.SetColor("_Color", new Color(1, 1, 1, 0.3f));
		
		NodeManager.Ins.RemoveAllNodes();
		selectedShape = shape;
		DisplayNodesForShape();
		
		foreach(Shape s in shapeList) {
			if(s != shape)
				s.Parent.renderer.material.SetColor("_Color", new Color(1, 1, 1, 0.3f));
		}
	}
	
	
	// Displays nodes on each Shape vertex
	private void DisplayNodesForShape() {
		if(selectedShape != null && selectedShape.Vertices.Count > 0) {
			foreach(Transform v in selectedShape.Vertices) {
				NodeManager.Ins.AssignNode(v, selectedShape.UpdateVertex, selectedShape.RemoveVertex);
			}
		}
	}
	
	
	// Draws a temporary node in between two existing nodes
	// When the user clicks left mouse button, it creates a new node at that position
	private void DrawTempNewNode() {
		int listPos = 0;
		Vector3 tempPos = Vector3.zero;
		float closestPoint = float.PositiveInfinity;
		
		for(int i = 0; i < selectedShape.Vertices.Count; i++) {
			Vector3 midPoint = (selectedShape.Vertices[i].position + selectedShape.Vertices[(i >= selectedShape.Vertices.Count - 1) ? 0 : i + 1].position) / 2f;
			Vector3 mousePos = AtlasEditor.AtlasCam.ScreenToWorldPoint(Input.mousePosition);
			mousePos.z = 0;
			midPoint.z = 0;
			
			float tempDistance = Mathf.Abs(Vector3.Distance(midPoint, mousePos));
			
			if(tempDistance < closestPoint) {
				listPos = i;
				closestPoint = tempDistance;
				tempPos = midPoint;
			}
		}
		
		NodeManager.Ins.ShowTempNode(tempPos);
		
		if(Input.GetMouseButtonDown(0)) {
			selectedShape.AddVertex(new Vector3(tempPos.x, tempPos.y, 0), listPos + 1);
			DisplayNodesForShape();
		}
	}
	
	
	public void DeleteShape(Shape shape) {
		shapeList.Remove(shape);
		DestroyImmediate (shape.Parent.gameObject);
		
		selectedShape = null;
		
		NodeManager.Ins.UpdateNodes();
	}
	
	
	private void CreateGLMat() {
		if( !GLMat ) {
	        GLMat = new Material( "Shader \"Lines/Colored Blended\" {" +
	            "SubShader { Pass { " +
	            "    Blend SrcAlpha OneMinusSrcAlpha " +
	            "    ZWrite Off Cull Off Fog { Mode Off } " +
	            "    BindChannels {" +
	            "      Bind \"vertex\", vertex Bind \"color\", color }" +
	            "} } }" );
	        GLMat.hideFlags = HideFlags.HideAndDontSave;
	        GLMat.shader.hideFlags = HideFlags.HideAndDontSave;
	    }
	}
	
	
	private void LoadAtlas(Atlas atlas) {
		CleanAtlas();
		
		currentAtlas = atlas;
		shapeList = atlas.Shapes;
		
		foreach(Shape shape in shapeList) {
			shape.SetLayer("Animator");
			shape.SetMaterial(imagePlaceholderMat);
			shape.Parent.parent = transform;
		}
		
		StartCoroutine(loadTexture(Application.dataPath + currentAtlas.TexturePath));
	}
	
	
	private void SetupCamera() {
		AtlasCam = gameObject.AddComponent<Camera>();
		
		AtlasCam.cullingMask = 1 << LayerMask.NameToLayer("Animator");
		AtlasCam.backgroundColor = new Color(0.8f, 0.8f, 0.8f);
		AtlasCam.orthographic = true;
		AtlasCam.transform.localPosition = new Vector3(0, 0, -100);
		AtlasCam.orthographicSize = 180;
	}
	
	
	// TODO: Separate to own class
	private void CreateAtlas() {
		CleanAtlas();
		currentAtlas = new Atlas();
	}
	
	
	private void CleanAtlas() {
		currentAtlas = null;
		selectedShape = null;
		DeselectShape();
		
		for(int i = 0; i < shapeList.Count; i++) {
			DestroyImmediate (shapeList[i].Parent.gameObject);
		}
		
		shapeList = new List<Shape>();
	}
	
	
	private void OnGUI() {
		GUI.skin = Skin;
		
		GUILayout.BeginArea(new Rect(0, 0, 120, Screen.height));
		
		if(GUILayout.Button("New Atlas")) {
			CreateAtlas();
		}
		
		if(GUILayout.Button("Load Atlas...")) {
			string path = EditorUtility.OpenFilePanel("Load Atlas", Application.dataPath, "atl");
			
			if(!string.IsNullOrEmpty(path))
				LoadAtlas(AtlasManager.Ins.LoadAtlas(path));
		}
		
		GUI.enabled = currentAtlas != null;
		
		
		if(GUILayout.Button("Save Atlas")) {
			AtlasManager.Ins.SaveAtlas(currentAtlas, PlayerPrefs.GetString("latestAtlasPath"));
		}
		
		
		if(GUILayout.Button("Save Atlas as...")) {
			string path = EditorUtility.SaveFilePanel("Save Atlas", Application.dataPath, "newAtlas", "atl");
			
			if(!string.IsNullOrEmpty(path))
				AtlasManager.Ins.SaveAtlas(currentAtlas, path);
		}
		
		
		if(GUILayout.Button("Load Texture...")) {
			string path = EditorUtility.OpenFilePanel("Load Texture", Application.dataPath, "png");
			
			if(!string.IsNullOrEmpty(path))
				StartCoroutine(loadTexture(path));
		}
		
		if(GUILayout.Button("Add Complex Shape")) {
			tempVertexList = new List<Vector3>();
			NodeManager.Ins.RemoveAllNodes();
			currentMode = Modes.ADD_COMPLEX_SHAPE;
			//selectedShape = new Shape(Shape.ShapeType.COMPLEX);
			//shapeList.Add(selectedShape);
		}
		
		if(GUILayout.Button("Add Simple Shape")) {
			NodeManager.Ins.RemoveAllNodes();
			currentMode = Modes.ADD_SIMPLE_SHAPE;
			//selectedShape = new Shape(Shape.ShapeType.SIMPLE);
			//shapeList.Add(selectedShape);
		}
		
		currentShapeViewMode = (ShapeViewMode)GUILayout.SelectionGrid((int)currentShapeViewMode, Enum.GetNames(typeof(ShapeViewMode)), 3);
		
		GUI.enabled = selectedShape != null;
		
		GUILayout.Space(20);
		GUILayout.Label("Name:");
		
		if(selectedShape != null)
			selectedShape.Name = GUILayout.TextField(selectedShape.Name);
		else
			GUILayout.TextField("");
		
		GUI.enabled = true;
		
		GUILayout.EndArea();
		
		shapeWindowRect = GUI.Window(0, shapeWindowRect, DrawShapeList, "Shapes");
	}
	
	
	Vector2 shapeWindowScrollPos;
	private void DrawShapeList(int windowID) {
		int i = 0;
		
		GUI.DragWindow(new Rect(0, 0, 150, 30));
		
		shapeWindowScrollPos = GUI.BeginScrollView(new Rect(5, 30, 135, 360), shapeWindowScrollPos, new Rect(0, 0, shapeWindowRect.width - 30, shapeList.Count * 105));
		
		foreach(Shape shape in shapeList) {
			Rect rect = new Rect(0, i * 105, 100, 100);
			
			if(shape.ShapeTexture != null) {
				if(GUI.Button(rect, shape.ShapeTexture)) {
					SelectShape(shape);
				}
			}
			
			i++;
		}
		
		GUI.EndScrollView();
	}
	
	
	IEnumerator loadTexture(string path) {
		imagePlaceholderMat.mainTexture = null;
		
		WWW www = new WWW("file:///" + path);
		
		yield return www;
		
		loadedTexture = www.texture;
		
		imagePlaceholderMat.SetTexture("_MainTex", loadedTexture);
		imagePlaceholder.renderer.material = imagePlaceholderMat;
		UpdateWorkAreaSize(new Vector3(loadedTexture.width, 1, loadedTexture.height) / 10f);
		
		if(currentAtlas != null) {
			currentAtlas.SetTexture(path);
			
			// TODO: Find better place for this!
			foreach(Shape s in shapeList) {
				s.ShapeTexture = ModelToTexture.Ins.CreateTexture(s.Parent);
			}
		}
	}
	
	
	#region GLDrawing
	private void DrawCustomShape() {		
		foreach(Shape shape in shapeList) {
			switch(currentShapeViewMode) {
				case ShapeViewMode.OUTLINE:
					GL.Begin(GL.LINES);
       				GL.Color(shape == selectedShape ? ColShapeSelected : ColShapeNormal);
				
					if(shape.Vertices != null && shape.Vertices.Count > 1) {		
						for(int i = 1; i < shape.Vertices.Count; i++) {
							GL.Vertex3(shape.Vertices[i - 1].position.x, shape.Vertices[i - 1].position.y, 10);
							GL.Vertex3(shape.Vertices[i].position.x, shape.Vertices[i].position.y, 10);
						}
						
						GL.Vertex3(shape.Vertices[shape.Vertices.Count - 1].position.x, shape.Vertices[shape.Vertices.Count - 1].position.y, 10);
						GL.Vertex3(shape.Vertices[0].position.x, shape.Vertices[0].position.y, 10);
					}
				
					break;
				
				case ShapeViewMode.WIREFRAME:
					GL.Begin(GL.LINES);
       				GL.Color(shape == selectedShape ? ColShapeSelected : ColShapeNormal);
				
					if(shape.TriangulatedIndices != null && shape.TriangulatedIndices.Count >= 3) {		
						for(int i = 2; i < shape.TriangulatedIndices.Count; i += 3) {
							GL.Vertex3(shape.Vertices[shape.TriangulatedIndices[i - 2]].position.x, shape.Vertices[shape.TriangulatedIndices[i - 2]].position.y, 1);
							GL.Vertex3(shape.Vertices[shape.TriangulatedIndices[i - 1]].position.x, shape.Vertices[shape.TriangulatedIndices[i - 1]].position.y, 1);
							
							GL.Vertex3(shape.Vertices[shape.TriangulatedIndices[i - 1]].position.x, shape.Vertices[shape.TriangulatedIndices[i - 1]].position.y, 1);
							GL.Vertex3(shape.Vertices[shape.TriangulatedIndices[i]].position.x, shape.Vertices[shape.TriangulatedIndices[i]].position.y, 1);
							
							GL.Vertex3(shape.Vertices[shape.TriangulatedIndices[i]].position.x, shape.Vertices[shape.TriangulatedIndices[i]].position.y, 1);
							GL.Vertex3(shape.Vertices[shape.TriangulatedIndices[i - 2]].position.x, shape.Vertices[shape.TriangulatedIndices[i - 2]].position.y, 1);
						}
					}
				
					break;
				
				case ShapeViewMode.MESH:
					GL.Begin(GL.TRIANGLES);
					GL.Color((shape == selectedShape ? ColShapeSelected : ColShapeNormal) - new Color(0, 0, 0, 0.5f));
				
					if(shape.TriangulatedIndices != null && shape.TriangulatedIndices.Count >= 3) {		
						for(int i = 2; i < shape.TriangulatedIndices.Count; i += 3) {
							GL.Vertex3(shape.Vertices[shape.TriangulatedIndices[i - 2]].position.x, shape.Vertices[shape.TriangulatedIndices[i - 2]].position.y, 1);
							GL.Vertex3(shape.Vertices[shape.TriangulatedIndices[i - 1]].position.x, shape.Vertices[shape.TriangulatedIndices[i - 1]].position.y, 1);
							GL.Vertex3(shape.Vertices[shape.TriangulatedIndices[i]].position.x, shape.Vertices[shape.TriangulatedIndices[i]].position.y, 1);
						}
					}
					break;
			}
			
			GL.End();
		
			// Draw the center
			GL.Begin(GL.LINES);
			GL.Color(Color.black);
			
			GL.Vertex3(shape.Pivot.x - 5, shape.Pivot.y, 2);
			GL.Vertex3(shape.Pivot.x + 5, shape.Pivot.y, 2);
			GL.Vertex3(shape.Pivot.x, shape.Pivot.y - 5, 2);
			GL.Vertex3(shape.Pivot.x, shape.Pivot.y + 5, 2);
			GL.End();
			
			
			// Draw temp shape
			if(currentMode == Modes.ADD_COMPLEX_SHAPE) {
				GL.Begin(GL.LINES);
				GL.Color((shape == selectedShape ? ColShapeSelected : ColShapeNormal) - new Color(0, 0, 0, 0.5f));
			
				if(tempVertexList.Count > 1) {		
					for(int i = 1; i < tempVertexList.Count; i++) {
						GL.Vertex3(tempVertexList[i - 1].x, tempVertexList[i - 1].y, 10);
						GL.Vertex3(tempVertexList[i].x, tempVertexList[i].y, 10);
					}
					
					GL.Vertex3(tempVertexList[tempVertexList.Count - 1].x, tempVertexList[tempVertexList.Count - 1].y, 10);
					GL.Vertex3(tempVertexList[0].x, tempVertexList[0].y, 10);
				}
				
				GL.End();
			}
			
			
			/*
			// Draw tempbone
			if(tempBonePos.Count > 0) {
				GL.Begin(GL.TRIANGLES);
				GL.Color(Color.white);
				
				float width = 2;
				Vector3 mouseToWorld = AnimCam.ScreenToWorldPoint(Input.mousePosition);
				mouseToWorld.z = tempBonePos[0].z;
				
				Vector3 angle = new Vector3(0, 0, Vector3.Angle(mouseToWorld  -tempBonePos[0], Vector3.right));
				
				Vector3 pos1 = UniArt.RotatePointAroundPivot(tempBonePos[0] + new Vector3(0, width, 0), tempBonePos[0], angle);
				GL.Vertex3(pos1.x, pos1.y, 3);
				
				pos1 = UniArt.RotatePointAroundPivot(tempBonePos[0] - new Vector3(0, width, 0), tempBonePos[0], angle);
				GL.Vertex3(pos1.x, pos1.y, 3);
				
				GL.Vertex3(mouseToWorld.x, mouseToWorld.y, 3);
				
				GL.End();
			}
			*/
		}
	}
	
	
	// Draws the Image area
	private void DrawWorkArea() {
		Vector3 area = imagePlaceholder.localScale * 5f;
		
        GL.Begin(GL.LINES);
        GL.Color(new Color(0.3f, 0.3f, 0.3f));
		
        GL.Vertex(new Vector3(-area.x, -area.z, 0));
		GL.Vertex(new Vector3(-area.x, area.z, 0));
		
		GL.Vertex(new Vector3(-area.x, area.z, 0));
		GL.Vertex(new Vector3(area.x, area.z, 0));
		
		GL.Vertex(new Vector3(area.x, area.z, 0));
		GL.Vertex(new Vector3(area.x, -area.z, 0));
		
		GL.Vertex(new Vector3(area.x, -area.z, 0));
		GL.Vertex(new Vector3(-area.x, -area.z, 0));
		
        GL.End();
	}
	
	
	private void OnPostRender() {
        if (!GLMat) {
            Debug.LogError("[Animator] GLMat not available!");
            return;
        }
		
        GL.PushMatrix();
        GLMat.SetPass(0);
	
		DrawWorkArea();
		DrawCustomShape();
		
        GL.PopMatrix();
    }
	#endregion
}
