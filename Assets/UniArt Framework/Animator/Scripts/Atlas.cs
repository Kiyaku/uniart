using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Atlas {
	// Path to the texture it uses
	private string texturePath;
	// JSON String
	private JSObject shapesData = new JSObject();
	private List<Shape> shapeList = new List<Shape>();
	
	
	public string TexturePath {
		get { return texturePath; }
	}
	
	
	public List<Shape> Shapes {
		get { return shapeList; }
	}
	
	
	public Atlas() {
	}
	
	
	public Atlas(string data) {
		shapesData = JSObject.FromJSON(data);
		
		texturePath = shapesData.Get<string>("texturePath");
		ArrayList shapes = shapesData.Get<ArrayList>("shapes");
		
		texturePath = texturePath.Substring(texturePath.IndexOf("Assets") + 6);
		
		foreach(JSObject shape in shapes) {
			ArrayList vertices = shape.Get<ArrayList>("vertices");
			List<Vector3> vertexList = new List<Vector3>();
			
			foreach(JSObject vertex in vertices) {
				vertexList.Add(new Vector3((float)vertex.Get<double>("x"), (float)vertex.Get<double>("y"), 0));
			}
			
			// TODO: Save/Load Shape Type
			Shape newShape = new Shape(Shape.ShapeType.COMPLEX, vertexList);
			newShape.MovePivot(newShape.Pivot);
			newShape.Name = shape.Get<string>("name");
			
			shapeList.Add(newShape);
		}
	}
	
	
	public void SetTexture(string path) {
		texturePath = path;
		
		shapesData.Set ("texturePath", texturePath);
	}
	
	
	public void SetShapes(List<Shape> list) {
		foreach(Shape shape in list) {
			if(!shapeList.Contains(shape))
				shapeList.Add(shape);
		}
		
		UpdateData();
	}
	
	
	private void UpdateData() {
		ArrayList tempList = new ArrayList();
		
		for(int i = 0; i < shapeList.Count; i++) {
			JSObject newShape = new JSObject();
			
			ArrayList vertices = new ArrayList();

			for(int k = 0; k < shapeList[i].Vertices.Count; k++) {
				JSObject vertex = new JSObject();
				
				vertex.Set ("x", shapeList[i].Vertices[k].position.x);
				vertex.Set ("y", shapeList[i].Vertices[k].position.y);

				vertices.Add(vertex);
			}
			
			newShape.Set ("vertices", vertices);
			newShape.Set ("name", shapeList[i].Name);
			tempList.Add(newShape);
		}
		
		
		shapesData.Set ("shapes", tempList);
	}
	
	
	public string GetAtlasData() {
		UpdateData();
		return shapesData.ToJSON();
	}
}
