using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/*
 * A shape is a part of the atlas map.
 * It represents one object from the atlas map
 * Shapes contain:
 * 	- List of 2D Vectors that represent the shape
 * 	- List of Bones (not sure how they work yet, need to be parented too)
*/
public class Shape {
	public List<Transform> Vertices = new List<Transform>();
	public List<Vector3> TriangulatedVertices = new List<Vector3>();
	public List<int> TriangulatedIndices = new List<int>();
	public List<Bone> Bones = new List<Bone>();
	public Transform Parent;
	public Vector3 Pivot;
	public Texture2D ShapeTexture;
	public ShapeType Type;
	public string Name = "Shape";
	
	
	public enum ShapeType {
		SIMPLE,
		COMPLEX
	}
	
	
	public Shape(ShapeType type, List<Vector3> vertices, List<Vector3> bonePos = null) {
		if(Parent == null) {
			if(type == ShapeType.SIMPLE && bonePos == null) {
				Debug.LogError("Can't create simple shape without bones!");
				return;
			}
			
			Parent = new GameObject(Name).transform;
			Parent.gameObject.AddComponent<ShapeMesh>();
			Parent.gameObject.GetComponent<ShapeMesh>().ShapePtr = this;
			Parent.tag = "Shape";
			Type = type;
			
			Bone bone = new Bone(Parent);
			
			foreach(Vector3 vertex in vertices) {
				CreateVertex(vertex, bone.Target);
			}
			
			RecalculateCenter();
			MovePivot(Pivot);
			
			// TODO: Check if we have bones and use that position
			bone.SetPosition(Pivot);
			bone.SetData(Vertices);
			Bones.Add(bone);
		}
	}
	
	
	// Set layer of Shape Mesh
	public void SetLayer(string layerName) {
		if(Parent != null) {
			Parent.gameObject.layer = LayerMask.NameToLayer(layerName);
		} else {
			Debug.LogWarning("No Parent assigned to shape!");
		}
	}
	
	
	// Assigns Material to shape
	public void SetMaterial(Material mat) {
		if(mat != null && Parent != null && Parent.renderer != null) {
			Parent.renderer.material = mat;
		} else {
			Debug.LogWarning("No Parent or Renderer assigned to shape!");
		}
	}
	
	
	// Creates a new vertex without triangulizing
	private void CreateVertex(Vector3 pos, Transform parent = null, int listPos = -1) {
		GameObject newVertex = new GameObject("Vertex " + Vertices.Count);
		newVertex.transform.position = pos;
		newVertex.transform.parent = parent == null ? Parent : parent;
		
		if(listPos == -1)
			Vertices.Add(newVertex.transform);
		else 
			Vertices.Insert(listPos, newVertex.transform);
		
		if(Vertices.Count > 4) {
			Type = ShapeType.COMPLEX;
		}
	}
	
	
	// Add a vertex at the end of the list
	// If listPos is taller than -1, it adds a vertex at that position
	public void AddVertex(Vector3 pos, int listPos = -1) {
		CreateVertex(pos, null, listPos);
		
		RecalculateCenter();
		MovePivot(Pivot);
	}
	
	
	// Move the center of the shape to a new position
	public void MovePivot(Vector3 newPos) {
		foreach(Transform vertex in Vertices) {
			vertex.position -= newPos - Parent.position;
		}
		
		Parent.position = newPos;
		Pivot = newPos;
		
		Triangulate();
	}
	
	
	// Set Pivot to center of vertices
	private void RecalculateCenter() {
		Vector3 center = Vector3.zero;
		
		foreach(Transform v in Vertices) {
			center += v.position;
		}
		
		Pivot = center / (float)Vertices.Count;
		
		//if(Bones.Count > 0)
		//	Bones[0].SetPosition(Pivot);
	}
	
	
	// Removes 1 vertex. If it has less than 3, destroy that shape
	public void RemoveVertex(Transform vertex) {
		if(Vertices.Count <= 3 || Type == ShapeType.SIMPLE) {
			ShapeManager.Ins.DeleteShape(this);
		} else {
			Vertices.Remove(vertex);
			MonoBehaviour.Destroy(vertex.gameObject);
			Triangulate();
		}
	}
	
	
	public void UpdateVertex() {
		RecalculateCenter();
		MovePivot(Pivot);
		Triangulate();
	}
	
	
	// Creates Triangulated vertices 
	public void Triangulate() {
		// Use the triangulator to get indices for creating triangles
		Vector2[] vertices = new Vector2[Vertices.Count];
		Vector2[] uvs = new Vector2[Vertices.Count];
		
		for(int i = 0; i < Vertices.Count; i++) {
			vertices[i] = new Vector2(Vertices[i].localPosition.x, Vertices[i].localPosition.y);
		}
		
        Triangulator tr = new Triangulator(vertices);
		TriangulatedIndices = new List<int>(tr.Triangulate());
		
        // Create the Vector3 vertices
		TriangulatedVertices = new List<Vector3>();
		
		for (int i = 0; i < Vertices.Count; i++) {
			TriangulatedVertices.Add(new Vector3(Vertices[i].localPosition.x, Vertices[i].localPosition.y, 0));
			uvs[i] = new Vector2((Vertices[i].position.x - 128f) / 256f, (Vertices[i].position.y - 128) / 256f);
        }
		
		// Create the mesh
        Mesh msh = new Mesh();
        msh.vertices 	= TriangulatedVertices.ToArray();
        msh.triangles 	= TriangulatedIndices.ToArray();
		msh.uv 			= uvs;
        msh.RecalculateNormals();
        msh.RecalculateBounds();
		
		MeshFilter filter = null;
 
		// TODO: Don't create mesh right away? 
        // Set up game object with mesh;
		if(Parent.GetComponent<MeshRenderer>() == null) {
	        Parent.gameObject.AddComponent(typeof(MeshRenderer));
			Parent.gameObject.AddComponent<MeshCollider>();
	        filter = Parent.gameObject.AddComponent(typeof(MeshFilter)) as MeshFilter;
			Parent.GetComponent<MeshCollider>().sharedMesh = msh;
		} else {
			filter = Parent.GetComponent<MeshFilter>();
			Parent.GetComponent<MeshCollider>().sharedMesh = msh;
		}
		
		filter.mesh = msh;
	}
}
