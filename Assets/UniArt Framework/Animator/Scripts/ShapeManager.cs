using UnityEngine;
using System.Collections;

public class ShapeManager : Singleton<ShapeManager> {
	public delegate void DeleteShapeDelegate(Shape shape);
	public static event DeleteShapeDelegate onDeleteShape;
	
	
	public void DeleteShape(Shape shape) {
		if(onDeleteShape != null)
			onDeleteShape(shape);
	}
}
