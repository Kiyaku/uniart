using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;


public class AnimatorEditor : Singleton<AnimatorEditor> {
	public GUISkin Skin;
	
	private Atlas currentAtlas;
	private List<Shape> shapeList = new List<Shape>();
	private Model currentModel;
	
	private Camera AnimCam;
	private bool displayDropDown = false;
	private Shape tempShape;
	private Material modelMat;
	private Material GLMat;
	
	
	private void Awake() {
		CreateGLMat();
		Initialize();
		PreLoadAtlas();
	}
	
	
	private void CreateGLMat() {
		if( !GLMat ) {
	        GLMat = new Material( "Shader \"Lines/Colored Blended\" {" +
	            "SubShader { Pass { " +
	            "    Blend SrcAlpha OneMinusSrcAlpha " +
	            "    ZWrite Off Cull Off Fog { Mode Off } " +
	            "    BindChannels {" +
	            "      Bind \"vertex\", vertex Bind \"color\", color }" +
	            "} } }" );
	        GLMat.hideFlags = HideFlags.HideAndDontSave;
	        GLMat.shader.hideFlags = HideFlags.HideAndDontSave;
	    }
	}
	
	
	private void Initialize() {
		AnimCam = gameObject.AddComponent<Camera>();
		
		//AnimCam.cullingMask = 1 << LayerMask.NameToLayer("Animator");
		AnimCam.backgroundColor = new Color(0.8f, 0.8f, 0.8f);
		AnimCam.orthographic = true;
		AnimCam.transform.localPosition = new Vector3(0, 0, -100);
		AnimCam.orthographicSize = 180;
	}
	
	
	// Checks if there is an available atlas map. If so, load it
	private void PreLoadAtlas() {
		if(PlayerPrefs.HasKey("latestAtlasPath")) {
			LoadAtlas(AtlasManager.Ins.LoadAtlas(PlayerPrefs.GetString("latestAtlasPath")));
		}
	}
	
	
	
	private void LoadAtlas(Atlas atlas) {
		if(currentModel == null) {
			GameObject newModel = new GameObject("Model");
			currentModel = newModel.AddComponent<Model>();
			modelMat = new Material(Shader.Find("Transparent/VertexLit"));
			StartCoroutine(loadTexture(Application.dataPath + atlas.TexturePath));
		}
		
		currentAtlas = atlas;
		shapeList = atlas.Shapes;
		
		foreach(Shape shape in shapeList) {
			shape.Parent.parent = currentModel.transform;
			shape.Parent.position = Vector3.zero;
			shape.SetMaterial(modelMat);
		}
	}
	
	
	private void Update() {
		ScanForShapes();
	}
	
	
	private void ScanForShapes() {
		Ray ray = AnimCam.ScreenPointToRay(Input.mousePosition);
		ray.direction = new Vector3(0, 0, 1);
		RaycastHit hit;
		
		if(Input.GetMouseButtonDown(0)) {
			if(Physics.Raycast(ray, out hit, 10000)) {
	            if(hit.transform.tag == "Shape") {
					foreach(Shape shape in shapeList) {
						if(shape.Parent == hit.transform) {
							StartCoroutine(MoveShape(hit.transform));
							return;
						}
					}
				}
			}
		}
	}
	
	
	private IEnumerator MoveShape(Transform shape) {
		Vector3 offset = shape.position - UniArt.MouseToWorld(AnimCam);
		Vector3 newPos = offset;
		
		while(Input.GetMouseButton(0)) {
			newPos = offset + UniArt.MouseToWorld(AnimCam);
			newPos.z = shape.position.z;
			
			shape.position = newPos;
			yield return null;
		}
	}
	
	
	// TODO: Make this a re-usable static method?
	IEnumerator loadTexture(string path) {
		modelMat.mainTexture = null;
		
		WWW www = new WWW("file:///" + path);
		
		yield return www;
		
		Texture2D loadedTexture = www.texture;
		
		modelMat.SetTexture("_MainTex", loadedTexture);
	}
	
	
	private void OnGUI() {
		GUI.skin = Skin;
		
		GUILayout.BeginArea(new Rect(0, 0, 120, Screen.height));
		
		if(GUILayout.Button("Load Atlas...")) {
			string path = EditorUtility.OpenFilePanel("Load Atlas", Application.dataPath, "atl");
			
			if(!string.IsNullOrEmpty(path))
				LoadAtlas(AtlasManager.Ins.LoadAtlas(path));
		}
		
		GUILayout.EndArea();
		
		
		GUILayout.BeginArea(new Rect(Screen.width - 100, 0, 100, Screen.height));
		
		if(shapeList != null && shapeList.Count > 0) {
			DrawShapeOrder(currentModel.transform, 0);
		}
		
		if(displayDropDown) {
			GUILayout.Space(100);
			
			if(GUILayout.Button("none")) {
				tempShape.Parent.parent = currentModel.transform;
				displayDropDown = false;
			}
			
			foreach(Shape shape in shapeList) {
				if(shape != tempShape) {
					if(GUILayout.Button(shape.Name)) {
						tempShape.Parent.parent = shape.Parent;
						displayDropDown = false;
						UpdateShapeOrder(currentModel.transform, 0);
					}
				}
			}
		}
		
		GUILayout.EndArea();
	}
	
	
	// TODO: Manually change model order
	private void UpdateShapeOrder(Transform target, int offset) {
		if(target.GetComponent<ShapeMesh>() != null) {
			offset++;
			target.position = new Vector3(target.position.x, target.position.y, offset);
		}
		
		foreach(Transform t in target) {
			if(t.GetComponent<ShapeMesh>() != null) {
				UpdateShapeOrder(t, offset);
			}
		}
	}
	
	
	// Draw a hierarchy of shapes
	private void DrawShapeOrder(Transform target, int offset) {
		if(target.GetComponent<ShapeMesh>() != null) {
			offset++;
			
			GUILayout.BeginHorizontal();
			GUILayout.Space(offset * 10);
			
			if(GUILayout.Button("^", GUILayout.MaxWidth(30))) {
			}
			
			if(GUILayout.Button("v")) {
			}
			
			if(GUILayout.Button(target.GetComponent<ShapeMesh>().ShapePtr.Name)) {
				tempShape = target.GetComponent<ShapeMesh>().ShapePtr;
				displayDropDown = true;
			}
			
			GUILayout.EndHorizontal();
		}
		
		foreach(Transform t in target) {
			if(t.GetComponent<ShapeMesh>() != null) {
				DrawShapeOrder(t, offset);
			}
		}
	}
	
	
	#region GL_DRAWING
	private void DrawPivots() {
		if(shapeList != null) {
			foreach(Shape shape in shapeList) {
				GL.Begin(GL.LINES);
				GL.Color(Color.black);
				
				GL.Vertex3(shape.Parent.position.x - 5, shape.Parent.position.y, 2);
				GL.Vertex3(shape.Parent.position.x + 5, shape.Parent.position.y, 2);
				GL.Vertex3(shape.Parent.position.x, shape.Parent.position.y - 5, 2);
				GL.Vertex3(shape.Parent.position.x, shape.Parent.position.y + 5, 2);
				GL.End();
			}
		}
	}
	
	
	private void OnPostRender() {
        if (!GLMat) {
            Debug.LogError("[Animator] GLMat not available!");
            return;
        }
		
        GL.PushMatrix();
        GLMat.SetPass(0);
		
		DrawPivots();
		
        GL.PopMatrix();
    }
	#endregion
}
