using UnityEngine;
using System.Collections;
using System.Collections.Generic;


// Two types:
// Joint bone = moving this affects vertices
// End Bone = doesn't do anything to the shape but can be used to add bones
public class Bone {
	public Transform Parent;
	public Transform Target;
	public List<Transform> Vertices = new List<Transform>();
	
	
	public Bone(Transform parent) {
		Target = new GameObject("Bone").transform;
		Target.parent = parent;
	}
	
	
	public void SetPosition(Vector3 pos) {
		Target.position = pos;
	}
	
	
	public void SetData(List<Transform> vertices, Transform parent = null) {
		Vertices = vertices;
		
		if(parent != null) {
			Parent = parent;
			Target.parent = Parent;
		}
	}
}
