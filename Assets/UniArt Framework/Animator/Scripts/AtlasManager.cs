using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;


// Manages Atlas for loading/saving
public class AtlasManager : Singleton<AtlasManager> {
	public Atlas LoadAtlas(string path) {
		StreamReader fileReader = new StreamReader(path);
		
		Atlas atlas = new Atlas(fileReader.ReadLine());
		
		fileReader.Close();
		
		PlayerPrefs.SetString("latestAtlasPath", path);
		
		return atlas;
	}
	
	
	public void SaveAtlas(Atlas atlas, string path) {
		StreamWriter fileWriter = null;
		fileWriter = File.CreateText(path);
					
		fileWriter.WriteLine(atlas.GetAtlasData());
		fileWriter.Close();	
		
		PlayerPrefs.SetString("latestAtlasPath", path);
	}
}
