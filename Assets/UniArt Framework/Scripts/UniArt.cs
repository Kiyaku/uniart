using UnityEngine;
using System.Collections;

public class UniArt : Singleton<UniArt> {
	public AtlasEditor atlasEditor;
	public AnimatorEditor animatorEditor;
	
	
	// Rotates a Vector around a given Pivot
	public static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles) {
		Vector3 dir = point - pivot; 
		dir = Quaternion.Euler(angles) * dir;
		point = dir + pivot; 
		return point; 
	}
	
	
	public void Update() {
		if(Input.GetKeyDown(KeyCode.F1)) {
			atlasEditor.gameObject.SetActiveRecursively(true);
			animatorEditor.gameObject.SetActiveRecursively(false);
		}
		
		if(Input.GetKeyDown(KeyCode.F2)) {
			atlasEditor.gameObject.SetActiveRecursively(false);
			animatorEditor.gameObject.SetActiveRecursively(true);
		}
	}
	
	
	public static Vector3 MouseToWorld(Camera cam) {
		return cam.ScreenToWorldPoint(Input.mousePosition);
	}
}
